import markov as mk

def main_against(model):

    pos_data = model["pos"]
    neg_data = model["neg"]

    #Run for all values and get the answers.
    pos_set = [x for x in map(lambda x: mk.get_prediction(model, x), pos_data)]
    neg_set = [x for x in map(lambda x: mk.get_prediction(model, x), neg_data)]

    # Using 0.0001 to avoid dividing by 0, to get true negative, we only keep the falses in the negative set.
    # The model is oblivious to negative or positive input.
    true_neg = float(max(len([x for x in neg_set if not x]), 0.0001))
    true_pos = float(max(len([x for x in pos_set if x]), 0.0001))
    false_neg = float(max(len(pos_set) - true_pos, 0.0001))
    false_pos = float(max(len(neg_set) - true_neg, 0.0001))

    total_pos = true_pos + false_pos
    total_neg = true_neg + false_neg

    metrics = dict(
        order=model["order"],
        specificity=true_neg / (true_pos + false_neg),
        sensitivity=true_pos / (true_neg + false_pos),
        precision=true_pos / (total_pos),
        negative_predictive_value=true_neg / (total_neg),
        accuracy=(true_pos + true_neg) / (total_neg + total_pos)
    )

    return metrics
    pass