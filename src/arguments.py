import argparse
from against import main_against
from dump import main_dump
from evaluate import main_evaluate


def init_args(argv):
    parser = argparse.ArgumentParser(argv[0])

    subs = parser.add_subparsers(help="Choose mode.", dest="action")

    eval_parser = subs.add_parser("evaluate", help="Evaluate a set of Data")

    eval_parser.add_argument("--coding", "-c", metavar="path", action="store", nargs='+',
                             dest="coding", required=True, help="List of coding files in fasta format.")
    eval_parser.add_argument("--noncoding", "-n", metavar="path", action="store", nargs='+', dest="noncoding",
                             required=True, help="List of non-coding files in fasta format.")
    eval_parser.add_argument("--no-transition", "-nt", metavar="float", default=0.001, action="store", type=float,
                             dest="no_transition", help="Probability value for non existant transition in the model.")
    eval_parser.add_argument("--folds", "-f", default=10, metavar="integer", action="store",
                             type=int, dest="folds", help="Number of folds.")
    eval_parser.add_argument("--order", "-k", default=5, metavar="integer", action="store",
                             type=int, dest="order", help="Order of Markovian chain.")

    model_parser = subs.add_parser(
        "dump", help="Dump a model")

    model_parser.add_argument("--coding", "-c", metavar="path", action="store", nargs='+',
                              dest="coding", required=True, help="List of coding files in fasta format.")
    model_parser.add_argument("--noncoding", "-n", metavar="path", action="store", nargs='+',
                              dest="noncoding", required=True, help="List of non-coding files in fasta format.")
    model_parser.add_argument("--destination", "-o", metavar="path",
                              dest="output", action="store", required=True, help="Destination_file")
    model_parser.add_argument("--order", "-k", default=5, metavar="integer", action="store", type=int,
                              dest="order", help="Order of Markovian chain.")
    model_parser.add_argument("--no-transition", "-nt", metavar="float", default=0.001, action="store", type=float,
                              dest="no_transition", help="Probability value for non existant transition in the model.",
                              required=False)

    against_parser = subs.add_parser(
        "against", help="Load a model and test it against a set of data"
    )

    against_parser.add_argument("--coding", "-c", metavar="path", action="store", nargs='+',
                                dest="coding", required=True, help="List of coding files in fasta format.")
    against_parser.add_argument("--noncoding", "-n", metavar="path", action="store", nargs='+', dest="noncoding",
                                required=True, help="List of non-coding files in fasta format.")
    against_parser.add_argument("--source", "-s", metavar="path", dest="source",
                                action="store", required=True, help="Source File")

    return parser.parse_args(argv[1:])


def parse_args(args):
    ret = dict()
    #"mode" key is a function pointer.
    if args.action == "evaluate":
        ret["mode"] = main_evaluate
        ret["order"] = args.order
        ret["folds"] = args.folds
        ret["no trans"] = args.no_transition
    elif args.action == "dump":
        ret["mode"] = main_dump(args.output)
        ret["order"] = args.order
        ret["folds"] = 1
        ret["no trans"] = args.no_transition
    elif args.action == "against":
        ret["mode"] = main_against
        ret["source"] = args.source
        ret["folds"] = 1
    ret["action"] = args.action
    ret["pos"] = args.coding
    ret["neg"] = args.noncoding
    return ret
