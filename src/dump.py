import markov as mk

def main_dump(output):
    def func(model):
        model["initial_pos"] = mk.find_initial_probabilities(dataset=model["pos"], order=model["order"])
        model["initial_neg"] = mk.find_initial_probabilities(dataset=model["neg"], order=model["order"])
        model["trans_pos"] = mk.find_transition_probabilities(dataset=model["pos"], order=model["order"])
        model["trans_neg"] = mk.find_transition_probabilities(dataset=model["neg"], order=model["order"])
        with open(output, "w") as fp:
            from json import dump
            dump(model, fp)
        print("Model dumping completed.")
        return None
    return func
