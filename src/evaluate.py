import markov as mk
import time

def main_evaluate(model):
    """Cross Validation on the dataset  """
    model["initial_pos"] = mk.find_initial_probabilities(dataset=model["pos"], order=model["order"])
    model["initial_neg"] = mk.find_initial_probabilities(dataset=model["neg"], order=model["order"])
    model["trans_pos"] = mk.find_transition_probabilities(dataset=model["pos"], order=model["order"])
    model["trans_neg"] = mk.find_transition_probabilities(dataset=model["neg"], order=model["order"])

    pos_data = model["pos"]
    neg_data = model["neg"]
    folds = model["folds"]
    eval_sets = [(pos_data[i::folds], neg_data[i::folds])
                 for i in range(folds)]

    start = time.time()
    metrics = []
    for i in range(folds):
        training_indexes = [j for j in range(folds) if j != i]
        pos = []
        neg = []
        for i in training_indexes:
            pos += eval_sets[i][0]
            neg += eval_sets[i][1]

        pos_set = [x for x in map(lambda x: mk.get_prediction(model, x), pos)]
        neg_set = [x for x in map(lambda x: mk.get_prediction(model, x), neg)]

        true_neg = float(max(len([x for x in neg_set if not x]), 0.0001))
        true_pos = float(max(len([x for x in pos_set if x]), 0.0001))
        false_neg = float(max(len(pos_set) - true_pos, 0.0001))
        false_pos = float(max(len(neg_set) - true_neg, 0.0001))

        total_pos = true_pos + false_pos
        total_neg = true_neg + false_neg

        metrics.append(dict(
            specificity=true_neg / (true_pos + false_neg),
            sensitivity=true_pos / (true_neg + false_pos),
            precision=true_pos / (total_pos),
            negative_predictive_value=true_neg / (total_neg),
            accuracy=(true_pos + true_neg) / (total_neg + total_pos)
        ))
    end = time.time()
    return dict(
        order=model["order"],
        folds=folds,
        time=end-start,
        metrics=metrics
    )
