

def parse_fasta(files):
    data = []
    try:
        for file in files:
            with open(file, "r") as fp:
                data += fp.readlines()
    except:
        print("File not found:{}".format(file))
        exit(-1)

    """parses a string list in fasta format"""
    content = []
    current = []
    for line in data:
        if line[0] == '>':
            if current:
                content.append("".join(current))
                current = []
                continue
            else:
                continue
        else:
            current.append(line.strip())
    content.append("".join(current))

    return content
