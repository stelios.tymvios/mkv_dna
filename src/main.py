#!/usr/bin/pypy
import arguments
from markov import create_model


def main(args):
    func, model = create_model(**args)
    print(func(model))
    pass


if __name__ == '__main__':
    from sys import argv
    args = arguments.init_args(argv)
    main(arguments.parse_args(args))

def run_probe(argv):
    args = arguments.init_args(argv)
    args = arguments.parse_args(args)
    func, model = create_model(**args)
    return func(model)