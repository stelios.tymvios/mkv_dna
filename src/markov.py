import fasta
from math import log


def create_model(**kwargs):
    """
    Create a model from scratch or load one through a source file
    """
    model = dict()
    if kwargs["action"] == "against":
        import json
        try:
            with open(kwargs["source"], "r") as fp:
                model = json.load(fp)
        except FileNotFoundError:
            print("FileNotFound:{}".format(kwargs["source"]))
            exit(-1)
    else:
        model = dict(
            order=kwargs["order"],
            folds=kwargs["folds"],
            no_trans=kwargs["no trans"],
            initial_pos=dict(),
            initial_neg=dict(),
            trans_pos=dict(),
            trans_neg=dict()
        )

    model["pos"] = fasta.parse_fasta(kwargs["pos"])
    model["neg"] = fasta.parse_fasta(kwargs["neg"])

    return kwargs["mode"], model


def find_initial_probabilities(dataset, order=1):
    probs = {}
    count = 0.0
    for sequence in dataset:
        #Create a collection of subsequences of length "order"
        subseqs = [sequence[i: i + order] for i in range(len(sequence) - order)]
        for sub in subseqs:
            count += 1
            if sub in probs:
                probs[sub] += 1.0
            else:
                probs[sub] = 1.0
    probs = {key: val / count for key, val in probs.items()}
    return probs


def find_transition_probabilities(dataset, order=1,minimum=0.001):
    probs = {}
    for sequence in dataset:
        subseqs = [sequence[i: i + order] for i in range(len(sequence) - order)]
        transitions = [sequence[i + order] for i in range(len(sequence) - order)]
        for sub, tran in zip(subseqs, transitions):
            if sub in probs:
                probs[sub][tran] += 1.0
            else:
                probs[sub] = {c: minimum for c in ['A', 'C', 'T', 'G']}
                probs[sub][tran] = 1.0

    return {key: {tr: p / sum(val.values()) for tr, p in val.items()} for key, val in probs.items()}


def get_prediction(model, x):
    order = model["order"]

    def get_initial(initial):
        return [log(initial[x[:order]]) if x[:order] in initial else model["no_trans"]]

    def get_trans(trans):
        subseqs = [x[i: i + order] for i in range(len(x) - order)]
        transitions = x[order::1]
        probs = []
        for t, seq in zip(transitions[1:], subseqs[1:]):
            if seq in trans:
                probs.append(log(trans[seq][t]))
            else:
                probs.append(log(model["no_trans"]))
        return probs

    pos = sum(get_trans(model["trans_pos"])+get_initial(model["initial_pos"]))
    neg = sum(get_trans(model["trans_neg"])+get_initial(model["initial_neg"]))

    return pos > neg
